class MailChimp
  attr_reader :mail_chimp

  def initialize
    @mail_chimp = Mailchimp::API.new('7ac619af7fe8c9cff8c32fea2fdeabcb-us13')
  end

  def subscribe(user)
    mail_chimp.lists.subscribe('4f74f9e279', {email: user.email}, {'SUBSCRIBE' => 'TRUE', 'ANSWER' => user.selected_place})
  rescue => e
    puts e.message
  end

  def resubscribe(email)
    update_user_info(email, {'SUBSCRIBE' => 'TRUE'})
  end

  def unsubscribe(email)
    update_user_info(email, {'SUBSCRIBE' => 'FALSE'})
  end

  def update_referrals(user)
    update_user_info(user.email, {'RCOUNT' => user.referrals.count})
  end

  def update_selected_place(user)
    update_user_info(user.email, {'ANSWER' => user.selected_place})
  end

  private

  def update_user_info(email, info)
    mail_chimp.lists.subscribe('4f74f9e279', {email: email}, info, 'html', true, true)
  rescue => e
    puts e.message
  end

end