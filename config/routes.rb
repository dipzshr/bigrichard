Prelaunchr::Application.routes.draw do

  mount Buttercms::Engine => '/blog'
  ActiveAdmin.routes(self)

  devise_for :admin_users, ActiveAdmin::Devise.config

  root :to => "users#new"

  resources :users, path: '' do
    member do
    end
    collection do
      get 'unsubscribe_mail'
      post 'create'
      get 'refer', path: 'refer-a-friend'
      put 'clear_email_in_cookie'
      get 'check_if_user_exists'
      get 'terms_and_conditions'
    end
  end


  unless Rails.application.config.consider_all_requests_local
    get '*not_found', to: 'users#redirect', :format => false
  end
end