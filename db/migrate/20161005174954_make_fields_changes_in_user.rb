class MakeFieldsChangesInUser < ActiveRecord::Migration
  def change
    change_column :users, :referrals_count, :integer, default: 0
  end
end
