class AddUnsubscribeFieldToUsers < ActiveRecord::Migration
  def change
    add_column :users, :unsubscribe_mail, :boolean, :default => false
  end
end
