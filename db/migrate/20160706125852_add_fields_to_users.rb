class AddFieldsToUsers < ActiveRecord::Migration
  def change
    add_column :users, :terms_accepted, :boolean, default: false
    add_column :users, :referrals_count, :integer 
  end
end
