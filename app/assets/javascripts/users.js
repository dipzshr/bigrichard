$(document).ready(function(){
  function scroll() {
    $('html, body').animate({
      scrollTop: $(".form-part").offset().top
    }, 2000);
  }

  $(".btn.take-me-button").click(function() {
    scroll();
  });

  $('.refer-part .down-arrow').click(function(){
    $('html, body').animate({
        scrollTop: $(".offer-part").offset().top
    }, 2000);
  });

  $('a.condoms-only').click(function(){
    $(this).addClass('hide');
    $('.form-buttons a.festival-prize').removeClass('hide');
    $('.form-part .form-description').text("Enter your email address below to be given the chance to try our very first condoms and win more Big Richard goods.");
    $('.form-part .terms-text').hide();
    $('.form-buttons .or-option').hide();
    $('form.sign-up-form .sign-up-button').text('GIMME A CONDOM');
    $('form.sign-up-form  #selected_place').hide()
                                    .removeAttr('required')
    $('form.actual-form #user_selected_place').val('')
    return false;
  });

  $('a.festival-prize').click(function(){
    $(this).addClass('hide');
    $('.form-buttons a.condoms-only').removeClass('hide');
    $('.form-part .form-description').text("We are all about big experiences, so to celebrate our launch we are giving you the chance to try our very first condoms and a win a trip for two to any music festival in the world in 2017 - Coachella, Glastonbury, Lollapalooza, Splendour in the Grass... you name it, we’ll take you there. Fill in your answers below to win.");
    $('.form-part .terms-text').show();
    $('.form-buttons .or-option').show();
    $('form.sign-up-form .sign-up-button').text('ENTER COMPETITION');
    $('form.sign-up-form  #selected_place').show()
                                    .attr('required', true)
    return false;
  });

  $('form.sign-up-form').bind('ajax:beforeSend', function(xhr){
    $('.form-buttons .btn').attr('disabled', true)
    var place_field = $(this).find('#selected_place');
    if(place_field.is(':visible')){
      $('form.actual-form #user_selected_place').val(place_field.val())
    }
    var email = $(this).find('#email').val()
    $('form.actual-form #user_email').val(email)
  });

  $('form.sign-up-form').bind('ajax:complete', function(xhr, res){
    response = JSON.parse(res.responseText)
    if(response.user_exists){
      $('.form-part .form-header').text("YOU HAVE ALREADY SIGNED UP");
      $('.form-part .form-description').text("You have already successfully signed up using this email address. Click continue to see if you have earn’t more Big Richard products.")
      $('.sign-up-button').text('CONTINUE')
        .attr('onclick', "$('form.actual-form #create_user_button').click()")
      $('.form-buttons .btn').attr('disabled', false)
      $('form.sign-up-form .input-box').hide()
      $('a.condoms-only').hide()
      $('.form-buttons a.festival-prize').addClass('hide')
      $('.form-part .terms-text').hide()
      $('.form-part .or-option').hide()
      $('.form-part .form-buttons').attr('style', 'margin-top: 0px')
    }else{
      $('form.actual-form').submit()
      return
    }
  });

  $('form.actual-form').bind('ajax:beforeSend', function(xhr){
    $('.form-buttons .btn').attr('disabled', true)
  })

  $('form.actual-form').bind('ajax:complete', function(xhr, res){
    response = JSON.parse(res.responseText)
    if(response.redirect){
      localStorage.setItem('enter_competition', response.enter_competition)
      window.location = '/refer-a-friend';
    }else{
      $('form.sign-up-form #email').focus()
      $('.form-buttons .btn').attr('disabled', false)
      $.each(response.error_fields, function(i, field){
        field = '.form-part .invalid-' + field
        $(field).removeClass('hide')
          .fadeOut(3000)
      })
      setTimeout(function(){
        $.each(response.error_fields, function(i, field){
          field = '.form-part .invalid-' + field
          $(field).removeAttr('style')
            .addClass('hide')
        })
      }, 3200);
    }
  });

  function showImages(el) {
    var windowHeight = jQuery( window ).height();
    $(el).each(function(){
      var thisPos = $(this).offset().top;

      var topOfWindow = $(window).scrollTop();
      if (topOfWindow + windowHeight - 200 > thisPos ) {
        $(this).addClass("fadeIn");
      }
    });
  }

  $(window).scroll(function() {
    showImages('.ads .image');
  });

  function pop_up_facebook_like() {
    fb_div = $('#facebook-like-div')
    if(localStorage.getItem('enter_competition') == 'true' && fb_div.length > 0 && fb_div.hasClass('active')){
      winH = $(window).height()
      pop_up_div = $('#facebook-like-div .pop-up-image')
      headerH = pop_up_div.height()
      contentH = headerH + 70
      topSpace = (winH - contentH) / 2
      pop_up_div[0].style.marginTop = topSpace + 'px'
      fb_div.fadeIn('medium')
    }
  }

  function remove_pop_up() {
    $('#facebook-like-div').removeClass('active')
    $('#facebook-like-div').fadeOut('medium')
    localStorage.removeItem('enter_competition')
  }

  $('#facebook-like-div .pop-up-image .pop-up-cancel').click(function(){
    remove_pop_up()
  })

  $('#facebook-like-div').click(function(event){
    event.stopPropagation();
  });

  window.onload = function(){
    centerDashboardContent();
    if(gon.email_clear){
      scroll();
    }
    pop_up_facebook_like()
    FB.Event.subscribe('edge.create', function(response) {
      remove_pop_up();
    });
  }

  /* resize background images */
  function backgroundResize(){
    var windowH = $(window).height();
    $(".background").each(function(i){
      var path = $(this);
      // variables
      var contW = path.width();
      var contH = path.height();
      var imgW = path.attr("data-img-width");
      var imgH = path.attr("data-img-height");
      var ratio = imgW / imgH;
      // overflowing difference
      var diff = parseFloat(path.attr("data-diff"));
      diff = diff ? diff : 0;
      // remaining height to have fullscreen image only on parallax
      var remainingH = 0;
      if(path.hasClass("parallax")){
        var maxH = contH > windowH ? contH : windowH;
        remainingH = windowH - contH;
      }
      // set img values depending on cont
      imgH = contH + remainingH + diff;
      imgW = imgH * ratio;
      // fix when too large
      if(contW > imgW){
        imgW = contW;
        imgH = imgW / ratio;
      }
      //
      path.data("resized-imgW", imgW);
      path.data("resized-imgH", imgH);
      path.css("background-size", imgW + "px " + imgH + "px");
    });
  }

  function centerDashboardContent(){
    var windowH = $(window).height();
    var middle = $('.middle-content')
    middle.removeClass('hide')
    var middleH = middle.height();
    var headerH = $('.header-logo').height();
    var halfScreen = parseInt((windowH - middleH) / 2);
    if(halfScreen > 2 * headerH){
      marginT =  halfScreen - headerH + 65;
    }
    else{
      marginT = 150
    }
    middle.css("margin-top", marginT + 'px');
  }

  $(window).resize(backgroundResize);
  $(window).resize(pop_up_facebook_like);
  $(window).resize(centerDashboardContent);
  $(window).focus(backgroundResize);
  $(window).focus(centerDashboardContent);

  backgroundResize();

  function zoomDisable(){
    $('head meta[name=viewport]').remove();
    $('head').prepend('<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0" />');
  }
  function zoomEnable(){
    $('head meta[name=viewport]').remove();
    $('head').prepend('<meta name="viewport" content="width=device-width, initial-scale=1" />');
  }

  $("input[type='email'], textarea").mouseup(zoomDisable)
    .mouseout(zoomEnable);


	// if ($(window).width() > 635) {
	// 	var lengthTwo = $('.circle').offset().left + 30;
	// 	var lengthThree = $('.circle:eq(1)').offset().left + 30;
	// 	var lengthFour = $('.circle:eq(2)').offset().left + 30;
	// 	var setProgress = $('.circle:eq(3)').offset().left - 50;
	// 	var image = '<div class="droplet-desktop">';
	// 	var width = $(window).width();
	// 	var margins = (width - setProgress) / 2;

	// 	$('.progress').css('width', setProgress);
	// 	$('.page-content').css('margin-left', margins);
	// 	$('.page-content').css('margin-right', margins);

	// 	if ($('#check').hasClass('prize-two')) {
	// 		$('.bar').css('width', lengthTwo);
	// 		$('.bar').append(image);
	// 	}
	// 	else if ($('#check').hasClass('prize-three')) {
	// 		$('.bar').css('width', lengthThree);
	// 		$('.bar').append(image);
	// 	}
	// 	else if ($('#check').hasClass('prize-four')) {
	// 		$('.bar').css('width', lengthFour);
	// 		$('.bar').append(image);
	// 	}
	// 	else if ($('#check').hasClass('prize-five')) {
	// 		$('.bar').css('width', setProgress);
	// 		$('.bar').append(image);
	// 	}
	// 	else {
	// 		$('.bar').css('width', 150);
	// 		$('.bar').append(image);
	// 	}
	// } else if ($(window).width() <= 635) {
	// 	var image = '<div class="droplet-mobile">';

	// 	$('.bar').append(image);

	// 	if ($('#check').hasClass('prize-two')) {
	// 		$('.droplet-mobile').css('margin-top', 85);
	// 	}
	// 	else if ($('#check').hasClass('prize-three')) {
	// 		$('.droplet-mobile').css('margin-top', 185);
	// 	}
	// 	else if ($('#check').hasClass('prize-four')) {
	// 		$('.droplet-mobile').css('margin-top', 295);
	// 	}
	// 	else if ($('#check').hasClass('prize-five')) {
	// 		$('.droplet-mobile').css('margin-top', 435);
	// 	}
	// }

	// 	$(window).on('resize', function(){
	// 		if ($(window).width() > 635) {
	// 			var lengthTwo = $('.circle').offset().left + 30;
	// 			var lengthThree = $('.circle:eq(1)').offset().left + 30;
	// 			var lengthFour = $('.circle:eq(2)').offset().left + 30;
	// 			var setProgress = $('.circle:eq(3)').offset().left - 50;
	// 			var image = '<div class="droplet-desktop">';
	// 			var width = $(window).width();
	// 			var margins = (width - setProgress) / 2;

	// 			$('.progress').css('width', setProgress);
	// 			$('.page-content').css('margin-left', margins);
	// 			$('.page-content').css('margin-right', margins);
				
	// 			if ($('#check').hasClass('prize-two')) {
	// 				$('.bar').css('width', lengthTwo);
	// 				$('.droplet-mobile').replaceWith(image);
	// 			}
	// 			else if ($('#check').hasClass('prize-three')) {
	// 				$('.bar').css('width', lengthThree);
	// 				$('.droplet-mobile').replaceWith(image);
	// 			}
	// 			else if ($('#check').hasClass('prize-four')) {
	// 				$('.bar').css('width', lengthFour);
	// 				$('.droplet-mobile').replaceWith(image);
	// 			}
	// 			else if ($('#check').hasClass('prize-five')) {
	// 				$('.bar').css('width', setProgress);
	// 				$('.droplet-mobile').replaceWith(image);
	// 			}
	// 			else {
	// 				$('.bar').css('width', 150);
	// 				$('.droplet-mobile').replaceWith(image);
	// 			}
	// 		}
	// 		else {
	// 			var image = '<div class="droplet-mobile">';

	// 			$('.bar').css('width', 4);
	// 			$('.page-content').css('margin', 0);
	// 			$('.droplet-desktop').replaceWith(image);

	// 			if ($('#check').hasClass('prize-two')) {
	// 				$('.droplet-mobile').css('margin-top', 85);
	// 			}
	// 			else if ($('#check').hasClass('prize-three')) {
	// 				$('.droplet-mobile').css('margin-top', 185);
	// 			}
	// 			else if ($('#check').hasClass('prize-four')) {
	// 				$('.droplet-mobile').css('margin-top', 295);
	// 			}
	// 			else if ($('#check').hasClass('prize-five')) {
	// 				$('.droplet-mobile').css('margin-top', 435);
	// 			}
	// 		}
	// 	});
});