# Export to CSV with the referrer_id
ActiveAdmin.register User do
  csv do
    column :id
    column :email
    column :referral_code
    column :referrer_id
    column :unsubscribe_mail
    column :email_token
    column :selected_place
    column :terms_accepted
    column :referrals_count
    column :created_at
    column :updated_at
  end

  actions :index, :show

  index do
    column :id
    column :email
    column :referral_code
    column :referrer_id
    column :unsubscribe_mail
    column :email_token
    column :selected_place
    column :terms_accepted
    column :referrals_count
    column :created_at
    column :updated_at
  end
  
end
