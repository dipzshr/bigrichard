module UsersHelper

  def get_class_for_progress
    referrals_count = @user.referrals.count
    class_name =
      if referrals_count >= 50
        'one-year-free-condoms'
      elsif referrals_count >= 20
        'mystery-sex-toy'
      elsif referrals_count >= 10
        'ten-pack-condoms'
      elsif referrals_count >= 5
        'three-pack-condoms'
      elsif referrals_count < 5
        'no-offers'
      end
  end

  def tolc_offers
    [
      {
        friends_count: 5,
        offer: 'Official Entry of<br>Competition'
      },
      {
        friends_count: 10,
        offer: '3 Pack<br>Condoms'
      },
      {
        friends_count: 20,
        # offer: '20 Pack<br>Condoms'
        offer: '10 Pack<br>Condoms'
      },
      {
        friends_count: 50,
        offer: '1 Year Worth of<br>Free Condoms'
      }
    ]
  end

  def get_class_for_offer(index, count)
    index == count -1 ?
      'col-xs-3'
      : 'col-xs-2'
  end

  def get_count_class(count)
    referrals_count = @user.referrals.count
    referrals_count >= count ?
      'reached' : 'not-reached'
  end

  def vertical_offer_margin_top
    ['44px', '35px', '40px', '40px']
  end

  def bar_height
    referrals_count = @user.referrals.count
    if referrals_count >= 50
      '310px'
    elsif referrals_count >= 20
      '220px'
    elsif referrals_count >= 10
      '150px'
    elsif referrals_count >= 5
      '80px'
    elsif referrals_count < 5
      '35px'
    end
  end

  def terms_list
    [
      "This is a game of skill. Chance plays no part in determining the winner. Each entry will be  individually judged, based upon individual creative merit. All entries must be an independent creation by the entrant and free of any claims that they infringe any third party rights. Entries must not have been published previously and/or have been used to win prizes in any other competitions.",
      "Winning entrant will have the right to nominate any music festival in the world to travel to, subject to ticket availability.",
      "Employees of the promoter and agencies associated with this promotion are ineligible to enter the competition, as are their immediate families.",
      "To enter, entrants must have submitted a valid email address and observe and comply with all competition and website terms and conditions.",
      "The judges reserve the right to disqualify any entrant submitting an entry which, in the opinion of the judges, includes objectionable content, including but not limited to profanity, or potentially insulting, scandalous, inflammatory or defamatory comments. The judges' decision will be final and no correspondence will be entered into.",
      "All entries become and remain the property of the promoter and agencies associated with this promotion (Big Richard Industries Pty Ltd).",
      "Winners of the competition must be 18 years old and over and residents of Australia, New Zealand, USA, UK, Canada or Japan.",
      "The promoter reserves the right to withdraw or amend the competition as necessary due to circumstances outside its control.",
      "The prize is non-transferable and non-redeemable for cash. Any attempt to resell or auction all or any part of this prize will result in an immediate cancellation of the prize. Prize cannot be exchanged for another itinerary. In the event that, for any reason whatsoever, a winner does not take an element of the prize at the time stipulated by the promoter then that element of the prize will be forfeited by the winner and cash will not be awarded in lieu of that element of the prize. Both parties must travel together.",
      "The promoter reserves the right to substitute any component of the prize for an itinerary of equal value at their discretion.",
      "Prize cannot be gifted to other persons in lieu of the winner not being able to travel. Should the winner not be able to complete all aspects of the itinerary within the period of prize validity, the remaining items shall be forfeited without any replacement for their value.",
      "The winner is required to obtain the relevant & suitable travel insurance at their own expense in order to be able to take the competition prize. It is the traveller's personal responsibility to ensure that they have valid documentation, including but not limited to,",
      "Valid passports, visas, and health requirements, which meet the requirements of immigration and other government authorities at every destination. Any fines, penalties, payments or expenditures incurred as a result of such documents not meeting the requirements of those authorities will be the sole responsibility of the traveller/s.",
      "Prize includes 2 X tickets to any music festival in the world, 2 X economy flights to nearby region or city where music festival is held and 2 X 2 nights accommodation near to where the chosen music festival is located. Supply of all aspects of prize are at discretion of the promoter. Unless expressly stated, all other expenses become the responsibility of the winner.",
      "Additional prizes such as condoms and other physical goods indicated on the prize page require that valid email addresses of real people (and not an additional email of the entrant) are submitted to Big Richard Industries. Big Richard, as the promoter, reserves the right to validate email addresses to be that of real people through a number of methods including but not limited to email communication and tracking of website visitation.",
      "If the winning entrant also qualifies for additional prizes by referring 5 or more email addresses subject to meeting all other criteria mentioned here, the winning entrant will qualify for $1,000 extra spending money.",
      "The promoter shall not be liable for any loss or damage whatsoever which is suffered (including, but not limited to, indirect or consequential loss) or for personal injury which is suffered or sustained, in connection with the prize, except for any liability which cannot be excluded by laws.",
      "The prize is subject to availability as determined by the promoter. The entry period is valid from June 2016 - Dec 2016.",
      "Promotion commences 16 June 2016. Entries close 5pm December 30 2016.",
      "By entering the competition, all entrants will be deemed to have accepted and agreed to be bound by these rules.",
      "The Promoter is the Big Richard Industries, Birrell St, NSW 2026 ABN 11141540829.",
      "The information you are asked to provide to Big Richard Industries and is personal information and is protected by the Privacy Act 1988. The Gallery will not disclose this information to other parties other than in accordance with the Privacy Act 1988.",
      "All entries become the property of the promoter. The promoter may use any content supplied excluding names for any commercial purpose, including future promotional marketing and publicity purposes. Any and all content will be published in an anonymised format or with first name and age, only.",
      "Independent financial advice should be sought as tax implications may arise as a result of accepting the prize.",
      "In the event that for any reason whatsoever a winner does not accept the prize at the time stipulated by the promoter then the prize will be forfeited by the winner and cash will not be awarded in lieu of the prize.",
      "Entries will be judged at the promoter’s office in a 2 week period following the competition close date. The promoter reserves the right to change the competition close date and will notify entrants in advance of the change.",
      "The winner will be notified in writing by email within thirty (30) working days of the conclusion of the competition.",
      "If the promoter is unable to contact the winner within 30 days of the draw and the promoter has made every effort to contact them with the information provided by the entrant then the promoter will award the prize to another valid entrant.",
      "It is a condition of entry into this competition that entrants agree to receive electronic communication from the promoter."
    ]
  end

end
