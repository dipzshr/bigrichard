module MailerHelper
  def ranges
    [
      {'start' => 1, 'end' => 5},
      {'start' => 6, 'end' => 10},
      {'start' => 11, 'end' => 20},
      {'start' => 21, 'end' => 50}
    ]
  end

  def referral_message
    referral_count = @user.referrals.count
    end_points = ranges.map{|r| r['end']}
    header_message = "<p style='font-size: 30px;'>Winner</p>" if end_points.include?(referral_count) || referral_count > 50
    if header_message
      if index = end_points.find_index(referral_count)
        next_milestone = end_points[index + 1]
      end
      next_prize_message = next_milestone ? " If #{next_milestone - referral_count} more people sign up, you will reach the next prize level." : ""
      body_message = "<p style='margin-top: 40px;'>#{referral_count} of your friends have signed up to the Big Richard VIP Pre-Launch email list. That means, you're awesome. And you've won a prize!<p style='margin-top: 40px;'>Visit our website to find out what you've won." + next_prize_message + "</p><a href='#{root_url}'>#{root_url}</a>"
    else
      range = ranges.detect{|r| referral_count.between?(r['start'], r['end'])}
      header_message = "<p style='font-size: 30px;'>#{referral_count}/#{range['end']} SIGNUPS</p>"
      body_message = "<p style='margin-top: 40px;'>Good news! #{referral_count} of your friends has signed up to the Big Richard VIP Pre-Launch email list.</p><p style='margin-top: 40px;'>You only have #{range['end'] - referral_count} more to go to win a prize!</p>"
    end
    header_message + body_message
  end

end