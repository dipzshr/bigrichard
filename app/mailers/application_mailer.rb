class ApplicationMailer < ActionMailer::Base
  default from: "hello@bigrichard.co"
  layout 'mailer'
  add_template_helper(MailerHelper)
end
