class Notifier < ApplicationMailer
  include SendGrid
  default from: 'Big Richard <hello@bigrichard.com.au>',
          return_path: 'hello@bigrichard.com.au'

  def signup_email(user)
    header.add_filter('template', 'enable', 1)
    header.add_substitution('-uniquecode-', [unique_code(user)])
    header.add_substitution('-host-', [host])
    header.add_substitution('-token-', [token(user)])
    header.add_filter('templates', 'template_id', welcome_template(user))
    mail = mail(user)
    mail.smtpapi = header
    client.send(mail)
  end

  def referral_alert(user)
    calculate_referrals(user)
    if @winner || @send_email
      header.add_filter('template', 'enable', 1)
      header.add_substitution('-uniquecode-', [unique_code(user)])
      header.add_substitution('-host-', [host])
      header.add_substitution('-token-', [token(user)])
      header.add_substitution('-referrals_count-', [@referrals_count])
      header.add_substitution('-remaining_referrals_count-', [@remaining_referrals_count])
      header.add_filter('templates', 'template_id', template_for_prize)
      mail = mail(user)
      mail.smtpapi = header
      client.send(mail)
    end
  end

  private

  def client
    @client ||= client = SendGrid::Client.new(api_user: 'BigRichardMail', api_key: '123Bigrichard!')
  end

  def mail(user)
    mail = SendGrid::Mail.new do |m|
      m.to = user.email
      m.to_name = ''
      m.from = "hello@bigrichard.com.au"
      m.from_name = "Big Richard"
      m.subject = 'To whom it may concern'
      m.html = 'Body'
    end
  end

  def header
    @header ||= Smtpapi::Header.new
  end

  def unique_code(user)
    domain = Rails.env.production? ?
      'http://www.bigrichard.co' : 'http://localhost:3000'
    domain + "/?ref=#{user.referral_code}"
  end

  def host
    Rails.env.production? ?
      'www.bigrichard.co' : 'localhost:3000'
  end

  def token(user)
    user.email_token
  end

  def ranges
    [
      {'start' => 1, 'end' => 5},
      {'start' => 6, 'end' => 10},
      {'start' => 11, 'end' => 20},
      {'start' => 21, 'end' => 50}
    ]
  end

  def calculate_referrals(user)
    @referrals_count = user.referrals.count
    end_points = ranges.map{|r| r['end']}
    if end_points.include?(@referrals_count)
      @winner = true
      next_milestone = end_points[end_points.find_index(@referrals_count) + 1]
      @remaining_referrals_count = next_milestone - @referrals_count
    else
      if @send_email = send_email?(@referrals_count)
        @remaining_referrals_count = remaining_count(@referrals_count)
      end
    end
  end

  def send_email?(referrals_count)
    referrals_count <= 10 || (referrals_count < 50 && referrals_count % 5 == 0)
  end

  def remaining_count(referrals_count)
    range = ranges.detect{|r| referrals_count.between?(r['start'], r['end'])}
    range['end'] - referrals_count
  end

  def template_for_prize
    @winner ? '74223eb7-c3fa-4b7d-af85-de1c0cac33b0' : '3e86f343-fe95-4f04-aa8e-4018e73c60cb'
  end

  def welcome_template(user)
    user.selected_place.present? ?
      'bd3bb2e3-037e-4887-a785-2f509f91aebe' : '7b11068e-2a3d-4e56-85c0-ab8d8311f275'
  end
end
