class User < ActiveRecord::Base
    belongs_to :referrer, :class_name => "User", :foreign_key => "referrer_id"
    has_many :referrals, :class_name => "User", :foreign_key => "referrer_id"

    validates :email, :uniqueness => true, :format => { :with => /\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*/i, :message => "Invalid email format." }
    validates :referral_code, :uniqueness => true
    validates :selected_place, length: {maximum: 255}
    before_save :accept_terms

    before_create :create_referral_code
    after_create :send_welcome_email
    # , :subscribe_to_mailchimp
    after_create :send_referral_alert, :if => lambda {|u| u.has_referral }
    after_create :update_referrals_count, :if => lambda {|u| u.referrer }

    REFERRAL_STEPS = [
        {
            'count' => 5,
            "html" => "TOLC Lube 50ml",
            "class" => "two",
        },
        {
            'count' => 10,
            "html" => "TOLC Lube 50ml<br>+ Condoms",
            "class" => "three",
        },
        {
            'count' => 15,
            "html" => "TOLC lube 100ml",
            "class" => "four",
        },
        {
            'count' => 20,
            "html" => "TOLC lube 100ml<br>+ Mystery Sex Toy",
            "class" => "five",
        }
    ]

    def has_referral
        referrer_id && referrer.check_subscription
    end

    def check_subscription
        !unsubscribe_mail
    end

    def create_email_token
        token = Digest::MD5.new
        hex = SecureRandom.hex(5)
        token << self.email + hex + Integer(Time.now).to_s
        self.update(email_token: token)
        yield
    end

    def send_welcome_email
        self.create_email_token do
            mail = Notifier.signup_email(self)
            mail.deliver_now
        end
    end

    private

    def create_referral_code
        referral_code = SecureRandom.hex(5)
        @collision = User.find_by_referral_code(referral_code)

        while !@collision.nil?
            referral_code = SecureRandom.hex(5)
            @collision = User.find_by_referral_code(referral_code)
        end

        self.referral_code = referral_code
    end

    def send_referral_alert
        self.referrer.create_email_token do
            mail = Notifier.referral_alert(self.referrer)
            mail.deliver_now
        end
    end

    # def subscribe_to_mailchimp
    #     MailChimp.new().subscribe(self)
    # end

    def update_referrals_count
        user = referrer
        count = referrer.referrals.count
        user.update(referrals_count: count)
        # MailChimp.new().update_referrals(self.referrer)
    end

    def accept_terms
        self.terms_accepted = selected_place.present?
        true
    end
end
