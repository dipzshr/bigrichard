class ApplicationController < ActionController::Base
    protect_from_forgery

    before_filter :ref_to_cookie
    before_filter :check_for_mobile

    def check_for_mobile
        prepare_for_mobile if mobile_device?
    end

    def prepare_for_mobile
        prepend_view_path Rails.root + 'app' + 'views_mobile'
    end

    def mobile_device?
        # Season this regexp to taste. I prefer to treat iPad as non-mobile.
        request.user_agent =~ /Mobile|webOS/ && request.user_agent !~ /iPad/
    end

    helper_method :mobile_device?

    protected

    def ref_to_cookie
        if params[:ref] && !Rails.application.config.ended
            if !User.find_by_referral_code(params[:ref]).nil?
                cookies[:h_ref] = { :value => params[:ref], :expires => 1.week.from_now }
            end

            if request.env["HTTP_USER_AGENT"] and !request.env["HTTP_USER_AGENT"].include?("facebookexternalhit/1.1")
                redirect_to proc { url_for(params.except(:ref)) }  
            end
        end
    end
end
